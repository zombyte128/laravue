<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pensamientos;
use Illuminate\Support\Facades\Auth;

class PensamientosController extends Controller
{
    public function __construct()
    {
       $this->middleware('auth');
    }

    public function index()
    {
        return Pensamientos::where('user_id',Auth::user()->id)->get();
    }

    
    public function store(Request $request)
    {
       $pensamientos = new Pensamientos();
       $pensamientos->descripcion = $request->descripcion;
       $pensamientos->user_id = Auth::user()->id;
       $pensamientos->save();
       return $pensamientos;
    }

   
    public function update(Request $request, $id)
    {
       $pensamientos = Pensamientos::findorFail($id);
       $pensamientos->descripcion = $request->descripcion;
       $pensamientos->user_id = Auth::user()->id;
       $pensamientos->save();
       return $pensamientos;
    }

    public function destroy($id)
    {
       $pensamientos = Pensamientos::findorFail($id);
       $pensamientos->delete();
    }
}
