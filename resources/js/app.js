require('./bootstrap');

window.Vue = require('vue');

Vue.component('pensamiento-component', require('./components/PensamientoComponent.vue').default);
Vue.component('form-component', require('./components/FormComponent.vue').default);
Vue.component('mispensamientos-component', require('./components/MisPensamientosComponent.vue').default);

const app = new Vue({
    el: '#app',
});
